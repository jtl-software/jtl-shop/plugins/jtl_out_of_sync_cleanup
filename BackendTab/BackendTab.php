<?php declare(strict_types=1);

namespace Plugin\jtl_out_of_sync_cleanup\BackendTab;

use JTL\DB\DbInterface;
use JTL\Plugin\Plugin;
use JTL\Services\DefaultServicesInterface;
use JTL\Services\JTL\AlertServiceInterface;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use SmartyException;

/**
 * Class BackendTab
 * @package Plugin\jtl_out_of_sync_cleanup\BackendTab
 */
abstract class BackendTab
{
    /** @var JTLSmarty */
    protected $smarty;

    /** @var Plugin */
    protected $plugin;

    /** @var DbInterface */
    protected $db;

    /** @var DefaultServicesInterface */
    protected $container;

    /** @var AlertServiceInterface */
    protected $alertService;

    /**
     * BackendTab constructor.
     *
     * @param JTLSmarty $smarty
     *
     */
    public function __construct(JTLSmarty $smarty, DbInterface $db)
    {
        $this->container    = Shop::Container();
        $this->smarty       = $smarty;
        $this->plugin       = $this->config->plugin;
        $this->db           = $db;
        $this->alertService = $this->container->getAlertService();

        if (!empty($_POST)) {
            $this->onPost();
        }
    }

    /**
     * will be used if there is any post-event to handle.
     */
    protected function onPost(): void
    {
    }

    /**
     * method for rendering a template.
     *
     * @return string
     *
     * @throws SmartyException
     */
    abstract public function render(): string;
}
